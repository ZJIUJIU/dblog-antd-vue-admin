import EditorJS from '@editorjs/editorjs';
import List from '@editorjs/list'; 



export default {
    data() {
        return {
            editor: null,
        }
    },
    mounted() {
        this.editor = new EditorJS({
            /**
             * Id of Element that should contain Editor instance
             */
            holder: 'editorjs',
            tools:{
                list:List
            }
        });
    },
    methods: {

    }
}
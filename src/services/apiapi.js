import { transformApi } from '@/services/api'
import { request, METHOD } from '@/utils/request'
import qs from 'qs'

const API = transformApi({
    TIMEUNITS: "/timeUnits",
    GETFILECONTENT: "/getFileContent",
    NOTICE: "/notice"
}, "/api");


export async function timeUnits() {
    return request(API.TIMEUNITS, METHOD.POST);
}


export async function getFileContent(path) {
    return request(API.GETFILECONTENT, METHOD.POST, qs.stringify({ path }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function notice(message) {
    return request(API.NOTICE, METHOD.POST, qs.stringify({ msg: message }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}
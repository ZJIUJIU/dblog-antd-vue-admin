import { transformApi } from '@/services/api'
import { request, METHOD } from '@/utils/request'
import qs from 'qs'

const API = transformApi({
    LIST: "/list",
    SAVE: "/save",
    REMOVE: "/remove",
    EDIT: "/edit",
    GET: "/get/{id}",
    UPDATE: "/update/{type}",
    LIST_ALL: "/listAll",
    BATCH_PUBLISH: "/batchPublish",
    PUSHTOBAIDU: "/pushToBaidu/{type}"
}, "/article");


/**
 * 文章列表，分页
 * 
 * @param name 账户名
 * @param password 账户密码
 * @param isRememberMe 记住我
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function articleList(keywords, title, searchText, sortOrder, pageSize, pageNumber) {

    // 参数列表，
    return request(API.LIST, METHOD.POST, qs.stringify({
        keywords: keywords,
        title: title,
        searchText: searchText,
        sortOrder: sortOrder,
        pageSize: pageSize,
        pageNumber: pageNumber,
    }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 删除文章
 * @param {Array} ids 文章得主键，id 
 * @returns 
 */
export async function deleteArticles(ids) {
    return request(API.REMOVE, METHOD.POST, qs.stringify({ ids }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 置顶、推荐、评论等操作的开关
 * @param {string} type [top\recommend\comment]
 * @param {number} id 
 * @returns 
 */
export async function updateTopOrRecommendedById(type, id) {
    return request(API.UPDATE.replace("{type}", type), METHOD.POST, qs.stringify({ type, id }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 保存文章
 * @param article 
 * @param tags 
 * @param file 
 * @returns 
 */
export async function save(article, tags, file) {
    tags = tags || article.tags || [];
    return request(API.SAVE, METHOD.POST, qs.stringify({ ...{ ...article, tags, file } }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 批量推送
 * @param {Array} ids 
 * @returns 
 */
export async function batchPublish(ids) {
    return request(API.BATCH_PUBLISH, METHOD.POST, qs.stringify({ ids }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 推送到百度
 * @param {Array} ids 
 * @returns 
 */
export async function pushToBaidu(ids) {
    return request(API.PUSHTOBAIDU.replace("{type}", "urls"), METHOD.POST, qs.stringify({ ids }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

/**
 * 更新文章id获取文章信息
 * @param  id 文章的主键
 * @returns 
 */
export async function get(id) {
    return request(API.GET.replace("{id}", id), METHOD.POST, qs.stringify(null, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}
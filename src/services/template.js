/**
 * 文章标签
 */
import { request, METHOD } from '@/utils/request'
import { transformApi } from "@/services/api";
import qs from 'qs'

const TAG_API = transformApi({
    LIST: "/list",
    ADD: "/add",
    REMOVE: "/remove",
    EDIT: "/edit",
    GET: "/get/{id}",
}, "/template");


/**
 * 文章列表，分页
 * 
 * @param condition @see {@link TypeConditionVO}
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function list(condition) {
    return request(TAG_API.LIST, METHOD.POST, qs.stringify(condition), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

/**
 * 删除文章分类
 * 
 * @param {Array}  ids 数组
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function remove(ids) {
    return request(TAG_API.REMOVE, METHOD.POST, qs.stringify({ ids }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}


export async function get(id) {
    return request(TAG_API.GET.replace("{id}", id), METHOD.POST, null, {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}


export async function add(tag) {
    return request(TAG_API.ADD, METHOD.POST, qs.stringify(tag, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}


export async function edit(tag) {
    return request(TAG_API.EDIT, METHOD.POST, qs.stringify(tag, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

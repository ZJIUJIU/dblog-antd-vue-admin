/**
 * 文章标签
 */
import { request, METHOD } from '@/utils/request'
import { transformApi } from "@/services/api";
import qs from 'qs'
// import { TypeConditionVO } from './vo/type'

const API = transformApi({
    RUN: "/run",
    STOP: "/stop",
    SINGLE: "/single",
    EXITWAYLIST: "/exitWayList",
    SPIDERCONFIG: "/spiderConfig",
    PLATFORMS: "/platforms"
}, "/remover");


export async function run(config) {
    return request(API.RUN, METHOD.POST, qs.stringify({ ...config }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function stop(notice) {
    return request(API.STOP, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function single(notice) {
    return request(API.SINGLE, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function exitWayList(notice) {
    return request(API.EXITWAYLIST, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function spiderConfig(notice) {
    return request(API.SPIDERCONFIG, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function platforms(notice) {
    return request(API.PLATFORMS, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}



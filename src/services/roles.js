/**
 * 文章标签
 */
import { request, METHOD } from '@/utils/request'
import { transformApi } from "@/services/api";
import qs from 'qs'
// import { TypeConditionVO } from './vo/type'

const API = transformApi({
    LIST: "/list",
    ADD: "/add",
    REMOVE: "/remove",
    EDIT: "/edit",
    GET: "/get/{id}",
    SAVEROLERESOURCES: "/saveRoleResources",
    ROLESWITHSELECTED: "/rolesWithSelected"
}, "/roles");


/**
 * 文章列表，分页
 * 
 * @param condition @see {@link TypeConditionVO}
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function list(condition) {

    return request(API.LIST, METHOD.POST, qs.stringify(condition), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

/**
 * 删除
 * 
 * @param {Array}  ids 数组
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function remove(ids) {
    return request(API.REMOVE, METHOD.POST, qs.stringify({ ids }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function add(notice) {
    return request(API.ADD, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function edit(notice) {
    return request(API.EDIT, METHOD.POST, qs.stringify(notice, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function get(id) {
    return request(API.GET.replace("{id}", id), METHOD.POST, qs.stringify(null, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function saveRoleResources(roleId, resourcesId) {
    return request(API.SAVEROLERESOURCES, METHOD.POST, qs.stringify({ roleId, resourcesId }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}

export async function rolesWithSelected(uid) {
    return request(API.ROLESWITHSELECTED, METHOD.POST, qs.stringify({ uid }, { arrayFormat: 'repeat' }), {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    });
}
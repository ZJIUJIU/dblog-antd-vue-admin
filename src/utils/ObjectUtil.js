
/**
 * 把origin中与target的同名的属性赋值给target
 * 这里应该有个递归，如果属性是对象，则应当递归赋值
 * 
 * @param {Object} origin
 * @param {Object} target
 */
function override(target, origin) {
    for (var prop in origin) {
        if (typeof (origin[prop]) !== "function"
            && prop in target
            && classOf(target[prop]) === classOf(origin[prop])) {
            if (classOf(origin[prop]) == "Object") {
                override(target[prop], origin[prop]);
            }
            else {
                target[prop] = origin[prop];
            }
        }
    }
    return target;
}


function classOf(o) {
    if (o === null) return "Null";
    if (o === undefined) return "Undefined";

    return Object.prototype.toString.call(o).slice(8, -1);
}

/**
 * 
 * @param {Object | Array} data 
 * @param {string} oldName 旧的属性名称
 * @param {string} newName 新的属性名称
 * @param {boolean} isDeleteOldName 是否删除旧的属性
 */
function keyReName(data, oldName, newName, isDeleteOldName = false) {
    if (classOf(data) == "Object") {
        keyReNameForObject(data, oldName, newName, isDeleteOldName)
    } else if (classOf(data) == "Array") {
        for (var item in data) {
            keyReNameForObject(data[item], oldName, newName, isDeleteOldName)
        }
    }
}

function keyReNameForObject(data, oldName, newName, isDeleteOldName = false) {

    if (classOf(data) !== "Object") {
        throw new Error("第一个参数data应该是一个Object！");
    }

    data[newName] = data[oldName];
    if (isDeleteOldName) {
        delete data[oldName];
    }
}


/**
 * 根据对象中的某个字段进行分级 , 目前只支持一级转多级
 * {a:{} , b:{parent:a}}
 * { a:{child:b} }
 * 
 * @param {Array} obj 
 * @param childObjKey 
 * @param parentKey 
 * @param isRemoveParent 
 */
// childObjKey = "children", parentKey = "parentId"
function listToTree(data, key = "id", childObjKey = "children", parentKey = "parentId") {
    // * 先生成parent建立父子关系
    const obj = {};
    data.forEach((item) => {
        obj[item[key]] = item;
    });

    const parentList = [];
    data.forEach((item) => {
        const parent = obj[item[parentKey]];
        if (parent) {
            // * 当前项有父节点
            parent.children = parent[childObjKey] || [];
            parent.children.push(item);
        } else {
            // * 当前项没有父节点 -> 顶层
            parentList.push(item);
        }
    });
    return parentList;
}


export {
    override,
    classOf,
    keyReName,
    listToTree
}
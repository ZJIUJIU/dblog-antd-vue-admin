import UpdateRecordList from "./UpdateRecordList";
import UpdateRecordEdit from "./UpdateRecordEdit";

export {
    UpdateRecordList,
    UpdateRecordEdit,
};
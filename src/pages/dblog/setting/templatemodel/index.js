import CustomTemplateModal from "./CustomTemplateModal";
import StorageNginxServerModal from "./StorageNginxServerModal";

export {
    CustomTemplateModal,
    StorageNginxServerModal
}
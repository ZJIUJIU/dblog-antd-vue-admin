import TemplateList from "./TemplateList";
import TemplateEdit from "./TemplateEdit";

export {
    TemplateList,
    TemplateEdit
};
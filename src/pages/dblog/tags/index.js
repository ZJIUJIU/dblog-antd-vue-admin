import TagsList from "./TagsList";
import TagsEdit from "./TagsEdit";

export {
    TagsList,
    TagsEdit
}
import TypeList from "./TypeList";
import TypeEdit from "./TypeEdit";

export {
    TypeList,
    TypeEdit
};
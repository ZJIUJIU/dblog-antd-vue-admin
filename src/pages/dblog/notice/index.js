import NoticeList from "./NoticeList";
import NoticeEdit from "./NoticeEdit";
export {
    NoticeList,
    NoticeEdit
};